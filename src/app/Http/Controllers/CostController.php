<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cost;
use App\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Budget;
class CostController extends Controller
{
    /**
     * Adding new cost item
     *
     * required:
     * @param string name
     * @param string price
     * @param integer category_id
     *
     * optional:
     * @param integer description
     *
     *
     * @throws Throwable
     *
     * @return json
     */
    public function addCost(Request $request)
    {
        $cost = new Cost();

        $validated = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required',
            'category_id' => 'required'
        ]);

        if ($validated->fails()) {
            return response()->json('Missed required fields', 400);
        }
        try {
            if ($request->has('date')) {
                $request->date = Carbon::parse($request->date)->format('Y-m-d');
            }else{
                $request->date = Carbon::now()->format('Y-m-d');
            }

            $cost->name = $request->name;
            $cost->price = $request->price;
            $cost->category_id = $request->category_id;
            $cost->date = $request->date;
            $cost->save();

            $budget = new Budget();
            $budget->addBudget(-$cost->price);
            return response()->json('success', 200);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    /**
     * Returning all category items
     *
     * @return json
     */
    public function getCategories()
    {
        $category = new Category();
        $allCategories = $category->all();
        return response()->json($allCategories,200);
    }

    /**
     * Returning cost items
     *
     * optional:
     * @param date $from
     * @param date $to
     *
     * @return json
     */
    public function getCostItems(Request $request)
    {
        // dd(Carbon::parse($request->from));
        $cost = new Cost();


        if ($request->from && $request->to) {
            $cost = $cost->whereBetween('date',[Carbon::parse($request->from),Carbon::parse($request->to)]);
        } else {
            $cost = $cost->take(10);
        }

        if ($request->category_id) {
            $cost = $cost->where('category_id', '=', $request->category_id);
        }

        $cost = $cost->with('categories')->orderBy('date', 'desc')->get();

        return response()->json($cost,200);
    }

    /**
     * Removing cost item by id
     * @param int $id
     *
     * @return json
     */
    public function deleteCostItem(int $id)
    {
        $cost = new Cost();
        $currentCost = $cost->find($id);
        if ($currentCost) {
            $budget = new Budget();
            $budget->addBudget($currentCost->price);
            $cost->find($id)->delete();
            return response()->json('Cost has been removed', 200);
        }else{
            return response()->json('Cost not found',404);
        }
    }

    /**
     * Main data for first app screen
     *
     * @return json
     */
    public function main()
    {
        $cost = new Cost();

        $todayPrice = $cost->whereDate('date',Carbon::today())->sum('price');
        $yesterdayPrice = $cost->whereDate('date',Carbon::yesterday())->sum('price');
        $todayDiff = (int) $todayPrice - (int) $yesterdayPrice;


        return response()->json([$todayPrice, $todayDiff],200);
    }
}
