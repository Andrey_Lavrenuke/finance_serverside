<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Budget;

class BudgetController extends Controller
{
    public function getBudget()
    {
        $budget = new Budget();
        return $budget->find(1)->first();
    }

    public function setBudget(Request $request)
    {
        $budget = new Budget();
        $budget = $budget->setBudget($request->budget);

        return response()->json($budget, 200);
    }

    public function addBudget(Request $request)
    {
        $budget = new Budget();
        $budget = $budget->addBudget($request->budget);
        return response()->json($budget, 200);
    }
}
