<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        for ($i=0; $i < 100; $i++) { 
            //make unique field hash
            try {
                Mail::send([], [], function ($message) use ($i){
                    $message->to('com.7f104ffa@dev.gasengineersoftware-mail.co.uk')
                    ->subject('[DEV][review-915-email-56l3rh] TEST SUBJ')
                    ->setBody("Test message #$i",'text/html')
                    ->setFrom('yapalq@gmail.com');
                });
                print_r('Email #'.$i . ' has been sent' . PHP_EOL);
            } catch (\Exception $th) {
                print_r($th->getMessage() . PHP_EOL);
            }
            sleep(5);
        }
    }
}
