<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    // protected $fillable = ['name','description','price','category_id'];

        /**
     * Get the comments for the blog post.
     */
    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
