<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Budget extends Model
{
    protected $table = 'budget';

    public function setBudget($count)
    {
        $budget = $this->first();
        $budget->budget = $count;
        $budget->save();
        return $budget->budget;
    }

    public function addBudget($count)
    {
        $budget = Budget::all()->first();
        $budget->budget += $count;
        $budget->save();
        return $budget->budget;
    }
}
