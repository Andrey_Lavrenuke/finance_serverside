<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('addCost','CostController@addCost');
Route::get('getCategories','CostController@getCategories');
Route::get('getCosts','CostController@getCostItems');

Route::delete('removeCost/{id}','CostController@deleteCostItem');

Route::get('main','CostController@main');

Route::get('getBudget', 'BudgetController@getBudget');

Route::post('setBudget', 'BudgetController@setBudget');
Route::post('addBudget', 'BudgetController@addBudget');





