<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('costs', 'date')) {
            Schema::table('costs', function (Blueprint $table){
                $table->date('date')->default(Carbon::now()->format('Y-m-d'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('costs', 'date'))
        {
            Schema::table('costs', function (Blueprint $table){
                $table->dropColumn('date');
            });
        }
    }
}
